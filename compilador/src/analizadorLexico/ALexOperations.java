package analizadorLexico;


import analizadorLexico.ClaseLexica;
import analizadorLexico.UnidadLexica;
import analizadorLexico.UnidadLexicaMultivaluada;
import analizadorLexico.UnidadLexicaUnivaluada;


import analizadorLexico.ClaseLexica; 
class ALexOperations {
	private AnalizadorLexico alex;

	public ALexOperations(AnalizadorLexico alex) {
		this.alex = alex;
	}

	public UnidadLexica unidadId() {

		return new UnidadLexicaMultivaluada(alex.fila(), ClaseLexica.IDEN, alex.lexema());

	};

	public UnidadLexica unidadBool() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.BOOL);
	};

	public UnidadLexica unidadNum() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.NUM);
	};

	public UnidadLexica unidadAnd() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.AND);
	};

	public UnidadLexica unidadOr() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.OR);
	};

	public UnidadLexica unidadNot() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.NOT);
	};

	public UnidadLexica unidadCierto() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.CIERTO);
	};

	public UnidadLexica unidadFalso() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.FALSO);
	};

	public UnidadLexica unidadNumero() {
		return new UnidadLexicaMultivaluada(alex.fila(), ClaseLexica.NUMERO,alex.lexema());
	}

	public UnidadLexica unidadMas() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.MAS);
	}

	public UnidadLexica unidadMenos() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.MENOS);
	}

	public UnidadLexica unidadPor() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.POR);
	}

	public UnidadLexica unidadDiv() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.DIV);
	}

	public UnidadLexica unidadPAp() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.PAP);
	}

	public UnidadLexica unidadPCierre() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.PCIERRE);
	}
	public UnidadLexica unidadIgual() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.IGUAL);
	}

	public UnidadLexica unidadPuntoYComa() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.PUNTOYCOMA);
	}

	public UnidadLexica unidadEof() { 
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.EOF);
	}

	public UnidadLexica unidadSeparador() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.SEPSEC);
	}

	public UnidadLexica unidadMayor() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.MAYOR);
	}

	public UnidadLexica unidadMenor() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.MENOR);
	}

	public UnidadLexica unidadMayorIgual() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.MAYORIGUAL);
	}

	public UnidadLexica unidadDistinto() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.DISTINTO);
	}

	public UnidadLexica unidadMenorIgual() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.MENORIGUAL);
	}

	public UnidadLexica unidadIgualIgual() {
		return new UnidadLexicaUnivaluada(alex.fila(), ClaseLexica.IGUALIGUAL);
	}
	public void error() {
	    System.err.println("***"+alex.fila()+" Caracter inexperado: "+alex.lexema());
	  }
}
