package analizadorSintactico; 

import errors.GestionErrores;
import java.io.IOException;
import java.io.Reader;

import analizadorLexico.AnalizadorLexico;
import analizadorLexico.ClaseLexica;
import analizadorLexico.UnidadLexica;


public class AnalizadorSintactico {
   private UnidadLexica anticipo;
   private AnalizadorLexico alex;
   private GestionErrores errores;
   public AnalizadorSintactico(Reader input) {
      errores = new GestionErrores();
      alex = new AnalizadorLexico(input);
      alex.fijaGestionErrores(errores);
      sigToken();
   }
   public void Sp() {
      P();
      empareja(ClaseLexica.EOF);
   }
   private void P() {
     switch(anticipo.clase()) {
         case BOOL: case NUM:  
        	  Decs();
        	  empareja(ClaseLexica.SEPSEC);
        	  Insts();
        	  
              break;
         default: errores.errorSintactico(anticipo.fila(),anticipo.clase(),
                                          ClaseLexica.BOOL,ClaseLexica.NUM);                                            
   }   
         
     
   }
   private void Inst() {
	   switch(anticipo.clase()) {
       case IDEN:
    	   asignacion();   	  
           break;       
       default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN);                                            
 }
	
   }
   
   private void Insts() {
	   switch(anticipo.clase()) {
       case IDEN:
    	   Inst();  
    	   Instsp();
           break;       
       default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN);                                            
 }
	
   }
   
   private void Instsp() {
	   switch(anticipo.clase()) {
       case PUNTOYCOMA:
    	   empareja(ClaseLexica.PUNTOYCOMA); 
    	   Inst();
    	   Instsp();
           break;   
       case EOF:break;
       default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.PUNTOYCOMA);                                            
 }
	
}
private void asignacion() {
	   switch(anticipo.clase()) {
	     case IDEN:
	  	   empareja(ClaseLexica.IDEN);   
	  	   empareja(ClaseLexica.IGUAL);  
	  	   Expr1();
	          break;       
	     default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN);
	   }
	
}
private void Expr1() {
	switch(anticipo.clase()) {
    case PAP: case IDEN:case NUMERO:case NOT: case MENOS: case FALSO: case CIERTO:
    	Expr2(); 
    	Expr1p();
         break;       
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.PAP,ClaseLexica.IDEN,ClaseLexica.NUMERO,ClaseLexica.CIERTO,ClaseLexica.FALSO,ClaseLexica.MENOS);
	}
	
}
private void Expr1p() {
	
	switch(anticipo.clase()) {
    case IGUALIGUAL: case MAYOR:case MENOR:case MAYORIGUAL:case MENORIGUAL:case DISTINTO:
    	OpN0(); 
    	Expr2();
         break; 
    case EOF: case PCIERRE: case PUNTOYCOMA: break;
    case OR:
    	OpN0or();
    	Expr1();
    	break;
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IGUALIGUAL,ClaseLexica.MAYOR,ClaseLexica.MENOR,ClaseLexica.EOF,ClaseLexica.PCIERRE,ClaseLexica.PUNTOYCOMA,ClaseLexica.OR,
    		ClaseLexica.MAYORIGUAL,ClaseLexica.MENORIGUAL,ClaseLexica.DISTINTO);
	}
	
}
private void OpN0or() {
	switch(anticipo.clase()) {
    case OR:  empareja(ClaseLexica.OR);break;     
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.OR);
	}
	
}
private void OpN0() {
	switch(anticipo.clase()) {
    case IGUALIGUAL:  empareja(ClaseLexica.IGUALIGUAL);break; 
    case MAYOR: empareja(ClaseLexica.MAYOR);break; 
    case MENOR: empareja(ClaseLexica.MENOR);break; 
    case MAYORIGUAL: empareja(ClaseLexica.MAYORIGUAL);break; 
    case MENORIGUAL: empareja(ClaseLexica.MENORIGUAL);break; 
    case DISTINTO: empareja(ClaseLexica.DISTINTO);break; 
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IGUALIGUAL,ClaseLexica.MAYOR,ClaseLexica.MENOR,
    		ClaseLexica.MAYORIGUAL,ClaseLexica.MENORIGUAL,ClaseLexica.DISTINTO);
	}
	
}
private void Expr2() {
	switch(anticipo.clase()) {
    case PAP: case IDEN:case NUMERO:case NOT: case MENOS: case FALSO: case CIERTO:
    	Expr3(); 
    	Expr2p();
         break;    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.PAP,ClaseLexica.IDEN,ClaseLexica.NUM,
    		ClaseLexica.NOT,ClaseLexica.CIERTO,ClaseLexica.FALSO,ClaseLexica.MENOS);
	}
	
}
private void Expr2p() {
	
	switch(anticipo.clase()) {
    case IGUALIGUAL: case MAYOR:case MENOR:case MAYORIGUAL:case DISTINTO:case MENORIGUAL:case OR:case EOF:case PCIERRE:case PUNTOYCOMA:
    	break;   	
        
    case MAS:case MENOS: 
    	OpN2();
    	Expr3();
    	Expr2p();break;
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IGUALIGUAL,ClaseLexica.MAYOR,ClaseLexica.MENOR,
    		ClaseLexica.MAYORIGUAL,ClaseLexica.MENORIGUAL,ClaseLexica.DISTINTO,ClaseLexica.OR,ClaseLexica.EOF,ClaseLexica.PCIERRE,ClaseLexica.PUNTOYCOMA);
	}
	
}
private void OpN2() {
	switch(anticipo.clase()) {
    case MAS:  
    	empareja(ClaseLexica.MAS);
    			break;   	
        
    case MENOS:
    	empareja(ClaseLexica.MENOS);
    			break;
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.MAS,	ClaseLexica.MENOS);
	}
	
}
private void Expr3() {
	switch(anticipo.clase()) {
    case IDEN: case NUMERO: case PAP: case NOT: case MENOS: case FALSO: case CIERTO:  
    	Expr4();
    	Expr3pp();
    	break;          
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN,	ClaseLexica.NUMERO,ClaseLexica.PAP,ClaseLexica.NOT,ClaseLexica.CIERTO,ClaseLexica.FALSO,ClaseLexica.MENOS);
	}
	
}
private void Expr3pp() {
	switch(anticipo.clase()) {
	case IGUALIGUAL: case MAYOR:case MENOR:case MAYORIGUAL:case DISTINTO:case MENORIGUAL:case IGUAL:case MAS:case PCIERRE:case PUNTOYCOMA:case EOF:case MENOS: case OR:case POR:case DIV:
    	
		Expr3p();
    	break;  
	case AND: OpN3and();Expr4();break;
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IGUALIGUAL,	ClaseLexica.MAYOR,ClaseLexica.MENOR,ClaseLexica.MAYORIGUAL,ClaseLexica.MAS,ClaseLexica.PCIERRE,ClaseLexica.PUNTOYCOMA
    		,ClaseLexica.DISTINTO,ClaseLexica.MENORIGUAL,ClaseLexica.IGUAL,ClaseLexica.EOF,ClaseLexica.MENOS,ClaseLexica.OR,ClaseLexica.POR,ClaseLexica.DIV);
	}
	
}
private void OpN3and() {
	switch(anticipo.clase()) {
	case AND:    empareja(ClaseLexica.AND);break;
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.AND);
	}
	
}
private void Expr3p() {
	switch(anticipo.clase()) {
	case DIV: case POR:
		OpN3();
		Expr4();
    	break;  
	case IGUALIGUAL: case MAYOR:case MENOR:case MAYORIGUAL:case DISTINTO:case MENORIGUAL:case IGUAL:case MAS:case PCIERRE:case PUNTOYCOMA:case OR:case MENOS: case EOF: break;
    
	default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.POR,	ClaseLexica.DIV,	ClaseLexica.IGUALIGUAL,	ClaseLexica.MAYOR,	ClaseLexica.MENOR,	ClaseLexica.MAYORIGUAL
			,	ClaseLexica.DISTINTO,	ClaseLexica.MENORIGUAL,	ClaseLexica.IGUAL,	ClaseLexica.MAS,	ClaseLexica.PCIERRE,	ClaseLexica.PUNTOYCOMA,	ClaseLexica.OR,	ClaseLexica.MENOS,	ClaseLexica.EOF);
	}
	
}
private void OpN3() {
	switch(anticipo.clase()) {
	case DIV:    empareja(ClaseLexica.DIV);break;
		
	case POR: empareja(ClaseLexica.POR);break;      
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.POR,	ClaseLexica.DIV);
	}
	
}
private void Expr4() {
	switch(anticipo.clase()) {
	case IDEN: case MENOS: case NUMERO: case PAP: case FALSO: case CIERTO:
		Expr5();
		break;
		
	case NOT:
		OpN4not();
		Expr4(); 
		break;      
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN,ClaseLexica.PAP,ClaseLexica.NUM,ClaseLexica.NOT,ClaseLexica.PCIERRE,ClaseLexica.FALSO,ClaseLexica.CIERTO,ClaseLexica.MENOS);
	}
	
}
/*private void Expr6() {
	switch(anticipo.clase()) {
	case IDEN: case NUMERO: case PAP:
		Literal();
		break;
		
	case MENOR:
		OpN6Unario();
		Literal(); 
		break;      
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN,ClaseLexica.PAP,ClaseLexica.NUM,ClaseLexica.MENOR);
	}
	
}*/
private void OpN5Unario() {
	 switch(anticipo.clase()) {
     case MENOS:    	      	 
    	 empareja(ClaseLexica.MENOS);
          break;
     default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.MENOS);  
	 }
	
}

private void Expr5() {
	switch(anticipo.clase()) {
	case IDEN: case NUMERO: case PAP: case CIERTO: case FALSO:
		Literal();
		break;
		
	case MENOS:
		OpN5Unario();
		Literal(); 
		break;      
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN,ClaseLexica.PAP,ClaseLexica.NUM,ClaseLexica.MENOS,ClaseLexica.CIERTO,ClaseLexica.FALSO);
	}
	
}
private void OpN4not() { 
	switch(anticipo.clase()) {
	case NOT:
		  empareja(ClaseLexica.NOT);		
		break;
    
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.NOT);
	}
	
}
private void Literal() {
	 switch(anticipo.clase()) {
     case IDEN:
  	   empareja(ClaseLexica.IDEN);  	  
          break;
     case NUMERO:
    	   empareja(ClaseLexica.NUMERO);  	  
            break;
     case PAP:
    	 empareja(ClaseLexica.PAP);
    	 Expr1();
    	 empareja(ClaseLexica.PCIERRE);  	  
            break;
     case CIERTO:
    	 empareja(ClaseLexica.CIERTO);    	 
            break;
     case FALSO:
    	 empareja(ClaseLexica.FALSO);    	 
         break;
     
     default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.IDEN, ClaseLexica.NUM, ClaseLexica.PAP,ClaseLexica.CIERTO,ClaseLexica.FALSO);                                            
}
	
}
private void Decsp() {
	   switch(anticipo.clase()) {
       case PUNTOYCOMA:
    	   empareja(ClaseLexica.PUNTOYCOMA);
    	   Dec();      	 
    	   Decsp();
            break;
       case SEPSEC: break;
       default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.PUNTOYCOMA,ClaseLexica.SEPSEC);                                            
 }
	
   }
private void Decs() {
	 switch(anticipo.clase()) {
     case BOOL: case NUM:  
  	   Dec();   	
  	   Decsp();
          break;
     default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.BOOL,ClaseLexica.NUM);                                            
}
	
}
private void Dec() {
	 switch(anticipo.clase()) {
     case BOOL: case NUM:  
  	  Tipo();      	 
  	 empareja(ClaseLexica.IDEN);
          break;
     default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.BOOL,ClaseLexica.NUM);  
	 }
	
}
private void Tipo() {
	switch(anticipo.clase()) {
    case BOOL:  	      	 
 	 empareja(ClaseLexica.BOOL);
         break;
    case NUM:
    	empareja(ClaseLexica.NUM);
        break;
    default: errores.errorSintactico(anticipo.fila(),anticipo.clase(), ClaseLexica.BOOL,ClaseLexica.NUM);  
	 }
	
}
   private void empareja(ClaseLexica claseEsperada) {
      if (anticipo.clase() == claseEsperada)
          sigToken();
      else errores.errorSintactico(anticipo.fila(),anticipo.clase(),claseEsperada);
   }
   private void sigToken() {
      try {
        anticipo = alex.yylex();
      }
      catch(IOException e) {
        errores.errorFatal(e);
      }
   }
   
}